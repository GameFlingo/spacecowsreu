﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    public Text scoreText;

    public int scoreValue;

    public Timer timerAdd;
    public bool menuTime = true;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + scoreValue;
    }

    //adding to score
    public void scoreAdd()
    {
        scoreValue += 100;
        SoundManagerScript.PlaySound("moo");

        if (scoreValue == 500)
        {
            //call timer to add time once player hits next level
            timerAdd.timerAddOnLevelup();
            SceneManager.LoadScene("Level 2");
        }

        if (scoreValue == 1000)
        {
            timerAdd.timerAddOnLevelup();
            SceneManager.LoadScene("Level 3");
        }

        if (scoreValue == 1500)
        {
            SceneManager.LoadScene("HighScore");
        }

    }

    



    
}
