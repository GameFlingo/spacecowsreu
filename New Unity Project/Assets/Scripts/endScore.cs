﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class endScore : MonoBehaviour
{

    public Text endScoreText;

    // Start is called before the first frame update
    void Start()
    {
        //set final score to end score multiplied by whatever the time left the player has when complete
        int finalScore = (int)Timer.timeLeft * 1500;
        endScoreText.text = "Final Score: " + finalScore; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
