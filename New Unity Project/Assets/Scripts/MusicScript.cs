﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour
{
    private static bool alreadyPlaying = false;

    // Start is called before the first frame update
    void Start()
    {
        //setting up the music loop so it doesnt replay overitself eternally
        if (alreadyPlaying == true)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        alreadyPlaying = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
