﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cowBounds1 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //set screen bounds for cow and player to hit
        float screenWidth = Camera.main.orthographicSize * 1.7777777f;
        float cowBoundaryRadius = 0.5f;
        Vector3 pos = transform.position;

        //Make sure cows dont clip through walls and exit screen

        if (pos.y + cowBoundaryRadius > Camera.main.orthographicSize)
        {
            pos.y = Camera.main.orthographicSize - cowBoundaryRadius;
        }

        if (pos.y - cowBoundaryRadius < -Camera.main.orthographicSize)
        {
            pos.y = -Camera.main.orthographicSize + cowBoundaryRadius;
        }

        if (pos.x + cowBoundaryRadius > screenWidth)
        {
            pos.x = screenWidth - cowBoundaryRadius;
        }

        if (pos.x - cowBoundaryRadius < -screenWidth)
        {
            pos.x = -screenWidth + cowBoundaryRadius;
        }

        transform.position = pos;
    }
}
