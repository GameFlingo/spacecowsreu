﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAdd : MonoBehaviour
{

    public Score scorePlus;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //call score script and add universally 
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name.Equals("pen"))
        {
            scorePlus.scoreAdd();

        }
    }
}
