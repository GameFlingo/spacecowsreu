﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    //switch scene script
    public Score ScorePlus;

    public void GotoMainScene()
    {
        SceneManager.LoadScene("Main Menu");
        
    }

    public void GotoLevel1Scene()
    {
        SceneManager.LoadScene("Level 1");
        
    }

    public void GotoHighScoreScene()
    {
        SceneManager.LoadScene("HighScore");
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}
