﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Lives : MonoBehaviour
{
    Text livesLefttxt;
    static float livesLeft = 3f;


    

    // Start is called before the first frame update
    void Start()
    {
        livesLefttxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        //set gameover state and reset lives to continue playability
        if (livesLeft <= 0)
        {
            SceneManager.LoadScene("Game Over");
            livesLeft = 3;
        }
        //display lives left on screen
        livesLefttxt.text = "Lives left: " + livesLeft;

        Scene currentScene = SceneManager.GetActiveScene();

        string sceneName = currentScene.name;

        if (sceneName == "Main Menu")
        {
            livesLeft = 3;
        }

        int buildIndex = currentScene.buildIndex;
    }

   
    //public class called in Timer.cs when player runs out of time to lose life
    public void loseLife()
    {
        livesLeft -= 1;
    }
}
