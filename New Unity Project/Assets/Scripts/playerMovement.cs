﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    float maxSpeed = 3f;
    float rotSpeed = 180f;
    float shipBoundaryRadius = 0.5f;



    // Update is called once per frame
    void Update()
    {
        //rotate ship
        Quaternion rot = transform.rotation;

        float z = rot.eulerAngles.z;

        z += Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;

        rot = Quaternion.Euler(0, 0, z);

        transform.rotation = rot;

        //move ship
        Vector3 pos = transform.position;

        Vector3 velocity = new Vector3(0, Input.GetAxis("Vertical") * maxSpeed, 0);


        GetComponent<Rigidbody2D>().velocity = rot * velocity;

        //keep player within game bounds

        float screenWidth = Camera.main.orthographicSize * 1.7777777f;

        if (pos.y + shipBoundaryRadius > Camera.main.orthographicSize)
        {
            pos.y = Camera.main.orthographicSize - shipBoundaryRadius;
        }

        if (pos.y - shipBoundaryRadius < -Camera.main.orthographicSize)
        {
            pos.y = -Camera.main.orthographicSize + shipBoundaryRadius;
        }

        if (pos.x + shipBoundaryRadius > screenWidth)
        {
            pos.x = screenWidth - shipBoundaryRadius;
        }

        if (pos.x - shipBoundaryRadius < -screenWidth)
        {
            pos.x = -screenWidth + shipBoundaryRadius;
        }


        transform.position = pos;


    }
    
}


