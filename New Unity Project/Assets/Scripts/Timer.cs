﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    Text text;
    public Lives lives;
    public static float timeLeft = 180f;
    public bool menuTime = true;


    void Start()
    {
        //set text
        text = GetComponent<Text>();
        //get current scene
        Scene currentScene = SceneManager.GetActiveScene();
        //set cuttent scene to a name
        string sceneName = currentScene.name;

        //set times per level
        if (sceneName == "Level 1")
        {
            timeLeft = 180;
        }

        if (sceneName == "Main Menu")
        {
            timeLeft = 600000000;
            
        }

        if (sceneName == "Game Over")
        {
            timeLeft = 600000000;
        }

        if (sceneName == "High Score")
        {
            timeLeft = 600000000;
        }




        int buildIndex = currentScene.buildIndex;


    }

    // Update is called once per frame
    void Update()
    {
        //setup timer per level and reload scene upon timeout, removing a life
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            timeLeft = 180;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            lives.loseLife();
        }
        text.text = "Time left: " + Mathf.Round(timeLeft);

        //setup button to be able to reset time
        if (Input.GetKeyDown(KeyCode.Space))
        {
            timeLeft = 180;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            lives.loseLife();
        }
    }

    //setup next level time add to be called in ScoreAdd.cs 
    public void timerAddOnLevelup()
    {
        timeLeft += 180;
    }

}
