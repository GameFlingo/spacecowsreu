﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{

    public static AudioClip moo;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        moo = Resources.Load<AudioClip>("moo");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "moo":
                audioSrc.PlayOneShot(moo);
                break;
        }
    }
   

    
}
